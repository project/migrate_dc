<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Kernel\Plugin\migrate\process;

use Drupal;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcStrToTime;
use Drupal\KernelTests\KernelTestBase;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcStrToTime<extended>
 */
class MigrateDcStrToTimeTest extends KernelTestBase implements MigrateMessageInterface {

  /**
   * {@inheritDoc}
   */
  public static $modules = [
    'migrate',
    'migrate_plus',
    'migrate_dc',
  ];

  /**
   * @var string
   */
  protected $pluginId = '';

  /**
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationPluginManager;

  /**
   * @var \Drupal\migrate\MigrateExecutable
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcStrToTime
   */
  protected $processor;

  /**
   * @var null
   */
  protected $destinationProperty;

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(static::$modules);
    $this->row = new Row();

    $this->migrationPluginManager = Drupal::service('plugin.manager.migration');
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationPluginManager->createStubMigration([]);
    $this->migrateExecutable = new MigrateExecutable(
      $migration,
      $this
    );
  }

  public function casesTransform(): array {
    return [
      'basic' => [
        [
          'value' => '1977-07-15T12:00:00',
          'requestTime' => strtotime('1977-07-12 12:00:00'),
          'valueToDateFormatter' => strtotime('1977-07-15 12:00:00'),
        ],
        [
          'output_format' => 'Y-m-d\TH:i:s',
          'base_time' => '1977-07-12 12:00:00',
        ],
        '+3 days',
      ],
      'unix' => [
        [
          'value' => 42,
          'requestTime' => strtotime('1977-07-12 12:00:00'),
          'valueToDateFormatter' => 42,
        ],
        [
          'output_format' => 'U',
        ],
        42,
      ],
    ];
  }

  /**
   * @dataProvider casesTransform
   */
  public function testTransform(array $expected, array $configuration, $value) {
    $expected += [
      'currentTime' => $expected['requestTime'],
    ];

    $container = Drupal::getContainer();
    $container->get('datetime.time');
    $container->get('date.formatter');

    $this->processor = MigrateDcStrToTime::create(
      $this->container,
      $configuration,
      $this->pluginId,
      $this->pluginDefinition
    );

    static::assertSame(
      $expected['value'],
      $this->processor->transform(
        $value,
        $this->migrateExecutable,
        $this->row,
        $this->destinationProperty
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function display($message, $type = 'status') {
    $this->assertTrue($type == 'status', $message);
  }

}
