<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Kernel\Plugin\migrate\process;

use Drupal;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcFileContent;
use Drupal\KernelTests\KernelTestBase;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcFileContent<extended>
 */
class MigrateDcFileContentTest extends KernelTestBase implements MigrateMessageInterface {

  /**
   * {@inheritDoc}
   */
  public static $modules = [
    'migrate_dc',
    'migrate',
    'file',
    'system',
    'user',
    'profile',
    'views',
  ];

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var string
   */
  protected $pluginId = '';

  /**
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * @var \Drupal\migrate\MigrateExecutable
   */
  protected $migrateExecutable;

  /**
   * @var null
   */
  protected $destinationProperty;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationPluginManager;

  /**
   * @var \Drupal\file\Entity\File
   */
  protected $file;

  /**
   * @var string
   */
  protected $fileContent = 'dummy text';

  /**
   * @var string
   */
  protected $filePath = 'public://';

  /**
   * @var string
   */
  protected $fileName = 'fileName.txt';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);

    $this->row = new Row();
    $this->destinationProperty = NULL;

    $this->migrationPluginManager = Drupal::service('plugin.manager.migration');
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationPluginManager->createStubMigration([]);
    $this->migrateExecutable = new MigrateExecutable(
      $migration,
      $this
    );

    file_save_data($this->fileContent, $this->filePath . $this->fileName);
  }

  public function casesTransformSuccess(): array {
    return [
      'file exists' => [
        $this->fileContent,
        [
          'path' => $this->filePath,
          'file_name' => $this->fileName,
        ],
        [
          'property' => '',
          'value' => '',
        ],
      ],
      'file exists with @ in path' => [
        $this->fileContent,
        [
          'path' => '@a/b/c',
          'file_name' => $this->fileName,
        ],
        [
          'property' => 'a/b/c',
          'value' => $this->filePath,
        ],
      ],
      'file does not exist' => [
        '',
        [
          'path' => 'foo',
          'file_name' => "bar",
        ],
        [
          'property' => '',
          'value' => '',
        ],
      ],
    ];
  }

  /**
   * @dataProvider casesTransformSuccess
   */
  public function testTransformSuccess($expected, $configuration, $destination) {
    $value = '';
    $this->row->setDestinationProperty($destination['property'], $destination['value']);

    $processor = new MigrateDcFileContent(
      $configuration,
      $this->pluginId,
      $this->pluginDefinition
    );

    static::assertSame(
      $expected,
      $processor
        ->transform(
          $value,
          $this->migrateExecutable,
          $this->row,
          $this->destinationProperty
      )
    );
  }

  public function casesTransformFail(): array {
    return [
      'no file in config' => [
        MigrateException::class,
        [],
        [
          'property' => '',
          'value' => '',
        ],
      ],
    ];
  }

  /**
   * @dataProvider casesTransformFail
   */
  public function testTransformFail($expected, $configuration, $destination) {
    $value = '';
    $this->row->setDestinationProperty($destination['property'], $destination['value']);

    $processor = new MigrateDcFileContent(
      $configuration,
      $this->pluginId,
      $this->pluginDefinition
    );

    $this->expectException($expected);
    $this->expectExceptionMessage('Config path value does not exist');

    $processor->transform(
      $value,
      $this->migrateExecutable,
      $this->row,
      $this->destinationProperty
    );
  }

  /**
   * {@inheritDoc}
   */
  public function display($message, $type = 'status') {
    $this->assertTrue($type == 'status', $message);
  }

}
