<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Kernel\Plugin\migrate\process;

use Drupal;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcShipmentItem;
use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;
use Drupal\KernelTests\KernelTestBase;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcShipmentItem<extended>
 */
class MigrateDcShipmentItemTest extends KernelTestBase implements MigrateMessageInterface {

  /**
   * {@inheritDoc}
   */
  public static $modules = [
    'system',
    'user',
    'options',
    'action',
    'views',
    'path',
    'path_alias',
    'token',
    'physical',
    'address',
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce',
    'commerce_store',
    'commerce_product',
    'commerce_order',
    'commerce_shipping',
    'commerce_price',
    'migrate',
    'migrate_dc',
  ];

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var string
   */
  protected $pluginId = '';

  /**
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationPluginManager;

  /**
   * @var \Drupal\migrate\MigrateExecutable
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\commerce_shipping\ShipmentItem
   */
  protected $shipmentItem;

  /**
   * @var \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcShipmentItem
   */
  protected $migrateShipmentItem;

  /**
   * @var null
   */
  protected $destinationProperty;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(static::$modules);
    $this->row = new Row();

    $this->migrationPluginManager = Drupal::service('plugin.manager.migration');
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationPluginManager->createStubMigration([]);
    $this->migrateExecutable = new MigrateExecutable(
      $migration,
      $this
    );

    $this->migrateShipmentItem = new MigrateDcShipmentItem(
      $this->configuration,
      $this->pluginId,
      $this->pluginDefinition
    );
  }

  public function casesTransform(): array {
    return [
      'basic' => [
        [
          new ShipmentItem([
            'order_item_id' => '32',
            'title' => 'a',
            'quantity' => 1,
            'weight' => new Weight('2', WeightUnit::KILOGRAM),
            'declared_value' => new Price('10', 'usd'),
          ]),
        ],
        [
          [
            'order_item_id' => '32',
            'title' => 'a',
            'quantity' => '1',
            'weight' => [
              'number' => '2',
              'unit' => WeightUnit::KILOGRAM,
            ],
            'declared_value' => [
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider casesTransform
   */
  public function testTransform($expected, $value) {
    static::assertEquals(
      $expected,
      $this->migrateShipmentItem->transform(
        $value,
        $this->migrateExecutable,
        $this->row,
        $this->destinationProperty
      )
    );
  }

  /**
   * {@inheritDoc}
   */
  public function display($message, $type = 'status') {
    $this->assertTrue($type == 'status', $message);
  }

}
