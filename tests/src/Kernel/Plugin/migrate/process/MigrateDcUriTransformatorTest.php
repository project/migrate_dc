<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Kernel\Plugin\migrate\process;

use Drupal;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcUriTransformator;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\KernelTests\KernelTestBase;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcUriTransformator<extended>
 */
class MigrateDcUriTransformatorTest extends KernelTestBase implements MigrateMessageInterface {

  public static $modules = [
    'system',
    'field',
    'filter',
    'text',
    'user',
    'node',
    'migrate',
    'migrate_dc',
  ];

  /**
   * @var string
   */
  protected $bundle = 'page';

  /**
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * @var \Drupal\migrate\MigrateExecutable
   */
  protected $migrateExecutable;

  /**
   * @var null
   */
  protected $destinationProperty;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationPluginManager;

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var string
   */
  protected $pluginId = '';

  /**
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\node\NodeTypeInterface
   */
  protected $nodeType;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $values = [
      'type' => $this->bundle,
      'name' => 'Page',
    ];
    $this->nodeType = NodeType::create($values);
    $this->nodeType->save();

    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);
    $this->row = new Row();
    $this->destinationProperty = NULL;

    $container = Drupal::getContainer();
    $this->entityTypeManager = $container->get('entity_type.manager');

    $this->migrationPluginManager = Drupal::service('plugin.manager.migration');
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationPluginManager->createStubMigration([]);
    $this->migrateExecutable = new MigrateExecutable(
      $migration,
      $this
    );
  }

  public function testTransform() {
    /** @var \Drupal\node\Entity\Node $node */
    $node = Node::create([
      'type' => $this->nodeType->bundle(),
      'title' => 'Test node',
      'uid' => 1,
    ]);
    $node->save();

    $expected = 'entity:node/' . $node->id();
    $value = 'entity.uuid:node/' . $node->uuid();

    $processor = new MigrateDcUriTransformator(
      $this->configuration,
      $this->pluginId,
      $this->pluginDefinition,
      $this->entityTypeManager
    );

    $actual = $processor->transform(
      $value,
      $this->migrateExecutable,
      $this->row,
      $this->destinationProperty
    );

    static::assertSame($expected, $actual);
  }

  /**
   * {@inheritDoc}
   */
  public function display($message, $type = 'status') {
    $this->assertTrue($type == 'status', $message);
  }

}
