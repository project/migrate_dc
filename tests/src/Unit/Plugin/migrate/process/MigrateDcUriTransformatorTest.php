<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\process;

use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\Tests\UnitTestCase;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcUriTransformator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcUriTransformator<extended>
 */
class MigrateDcUriTransformatorTest extends UnitTestCase {

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var string
   */
  protected $pluginId = '';

  /**
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * @var \Drupal\Core\Entity\EntityType|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityType;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\migrate\MigrateExecutableInterface
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * @var string
   */
  protected $destinationProperty = '';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityType = $this->createMock(EntityType::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManager::class);
    $this
      ->entityTypeManager
      ->method('getDefinition')
      ->willReturn($this->entityType);

    /** @var \Drupal\migrate\MigrateExecutable $migrateExecutable */
    $this->migrateExecutable = $this->createMock(MigrateExecutable::class);
    $this->row = new Row();
  }

  public static function casesTransformOriginal() {
    return [
      'entity' => [
        'entity:node/42',
        'entity:node/42',
      ],
    ];
  }

  /**
   * @dataProvider casesTransformOriginal()
   */
  public function testTransformOriginal(string $expected, string $value) {
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface|\PHPUnit\Framework\MockObject\MockObject $container */
    $container = $this->createMock(ContainerInterface::class);
    $container
      ->expects($this->once())
      ->method('get')
      ->with('entity_type.manager')
      ->willReturn($this->entityTypeManager);

    $processor = MigrateDcUriTransformator::create(
      $container,
      $this->configuration,
      $this->pluginId,
      $this->pluginDefinition
    );

    $actual = $processor->transform(
      $value,
      $this->migrateExecutable,
      $this->row,
      $this->destinationProperty
    );

    static::assertSame($expected, $actual);
  }

  public static function casesTransform(): array {
    return [
      'entity.uuid exists' => [
        'entity:node/42',
        'entity.uuid:node/my-uuid-01',
        '42',
      ],
      'entity.uuid not exists' => [
        'entity.uuid:node/my-uuid-01',
        'entity.uuid:node/my-uuid-01',
        NULL,
      ],
    ];
  }

  /**
   * @dataProvider casesTransform()
   */
  public function testTransform(string $expected, string $value, ?string $entityId) {
    /** @var \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcUriTransformator|\PHPUnit\Framework\MockObject\MockObject $processor */
    $processor = $this
      ->getMockBuilder(MigrateDcUriTransformator::class)
      ->setConstructorArgs([
        $this->configuration,
        $this->pluginId,
        $this->pluginDefinition,
        $this->entityTypeManager,
      ])
      ->onlyMethods(['getEntityIdFromUuid'])
      ->getMock();

    $processor
      ->method('getEntityIdFromUuid')
      ->with('node', 'my-uuid-01')
      ->willReturn($entityId);

    $actual = $processor->transform(
      $value,
      $this->migrateExecutable,
      $this->row,
      $this->destinationProperty
    );

    static::assertSame($expected, $actual);
  }

}
