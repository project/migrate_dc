<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\process;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcShipmentItem;
use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;
use Drupal\Tests\UnitTestCase;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcShipmentItem<extended>
 */
class MigrateDcShipmentItemTest extends UnitTestCase {

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    if (!class_exists(ShipmentItem::class)) {
      $this->markTestIncomplete();
    }

    parent::setUp();
  }

  public function casesTransform(): array {
    return [
      'basic' => [
        [
          new ShipmentItem([
            'order_item_id' => '32',
            'title' => 'a',
            'quantity' => 1,
            'weight' => new Weight(2, WeightUnit::KILOGRAM),
            'declared_value' => new Price('10', 'USD'),
          ]),
        ],
        [
          [
            'order_item_id' => '32',
            'title' => 'a',
            'quantity' => 1,
            'weight' => [
              'number' => 2,
              'unit' => WeightUnit::KILOGRAM,
            ],
            'declared_value' => [
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider casesTransform
   */
  public function testTransform($expected, $value) {
    $pluginId = '';
    $pluginDefinition = [];
    $configuration = [];

    /** @var \Drupal\migrate\MigrateExecutable $migrateExecutable */
    $migrateExecutable = $this->createMock(MigrateExecutable::class);
    $row = new Row();
    $destinationProperty = NULL;

    $migrateShipmentItem = new MigrateDcShipmentItem(
      $configuration,
      $pluginId,
      $pluginDefinition
    );

    static::assertEquals(
      $expected,
      $migrateShipmentItem->transform(
        $value,
        $migrateExecutable,
        $row,
        $destinationProperty
      )
    );
  }

}
