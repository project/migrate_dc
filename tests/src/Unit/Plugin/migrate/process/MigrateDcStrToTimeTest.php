<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\process;

use Drupal\Component\Datetime\Time;
use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcStrToTime;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcStrToTime<extended>
 */
class MigrateDcStrToTimeTest extends UnitTestCase {

  public function casesTransform(): array {
    return [
      'basic' => [
        [
          'value' => 'date-formatter-return',
          'requestTime' => strtotime('1977-07-12 12:00:00'),
          'valueToDateFormatter' => strtotime('1977-07-15 12:00:00'),
        ],
        [
          'output_format' => 'Y-m-d\TH:i:s',
        ],
        '+3 days',
      ],
      'unix' => [
        [
          'value' => 42,
          'requestTime' => strtotime('1977-07-12 12:00:00'),
          'valueToDateFormatter' => 42,
        ],
        [
          'output_format' => 'U',
        ],
        42,
      ],
    ];
  }

  /**
   * @dataProvider casesTransform
   */
  public function testTransform(array $expected, array $configuration, $value) {
    $plugin_id = '';
    $plugin_definition = [];

    $expected += [
      'currentTime' => $expected['requestTime'],
    ];

    /** @var \Drupal\Component\Datetime\Time|\PHPUnit\Framework\MockObject\MockObject $time */
    $time = $this->createMock(Time::class);
    $time
      ->method('getRequestTime')
      ->willReturn($expected['requestTime']);
    $time
      ->method('getCurrentTime')
      ->willReturn($expected['currentTime']);

    /** @var \Drupal\Core\Datetime\DateFormatterInterface|\PHPUnit\Framework\MockObject\MockObject $dateFormatter */
    $dateFormatter = $this->createMock(DateFormatterInterface::class);
    $dateFormatter
      ->method('format')
      ->with(
        $expected['valueToDateFormatter'],
        $configuration['output_type'] ?? 'custom',
        $configuration['output_format'] ?? 'U',
        $configuration['output_timezone'] ?? NULL,
        $configuration['output_langcode'] ?? NULL
      )
      ->willReturn('date-formatter-return');

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface|\PHPUnit\Framework\MockObject\MockObject $container */
    $container = $this->createMock(Container::class);
    $container
      ->method('get')
      ->willReturnMap([
        ['date.formatter', ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE, $dateFormatter],
        ['datetime.time', ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE, $time],
      ]);

    $processor = MigrateDcStrToTime::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    /** @var \Drupal\migrate\MigrateExecutable $migrateExecutable */
    $migrateExecutable = $this->createMock(MigrateExecutable::class);
    $row = new Row();
    $destinationProperty = NULL;

    static::assertSame(
      $expected['value'],
      $processor->transform(
        $value,
        $migrateExecutable,
        $row,
        $destinationProperty
      )
    );
  }

}
