<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcPlainPassword;
use Drupal\Tests\UnitTestCase;
use Drupal\Core\Password\PasswordInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcPlainPassword<extended>
 */
class MigrateDcPlainPasswordTest extends UnitTestCase {

  public function casesTransform(): array {
    return [
      'with value and config' => [
        'hash-of-value',
        [
          'default_value' => 'asdf',
        ],
        'asd',
      ],
      'empty value with config' => [
        'hash-of-config-default-value',
        [
          'default_value' => 'qwer',
        ],
        '',
      ],
      'empty value without config' => [
        'hash-of-hardcoded-default-value',
        [],
        '',
      ],
    ];
  }

  /**
   * @dataProvider casesTransform
   */
  public function testTransform(string $expected, array $configuration, $value) {
    $pluginId = '';
    $pluginDefinition = [];

    /** @var \Drupal\Component\Datetime\Time|\PHPUnit\Framework\MockObject\MockObject $passwordChecker */
    $passwordChecker = $this->createMock(PasswordInterface::class);
    $map = [
      [
        $value,
        'hash-of-value',
      ],
      [
        'a',
        'hash-of-hardcoded-default-value',
      ],
    ];

    if (array_key_exists('default_value', $configuration)) {
      $map[] = [
        $configuration['default_value'],
        'hash-of-config-default-value',
      ];
    }

    $passwordChecker
      ->method('hash')
      ->willReturnMap($map);

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface|\PHPUnit\Framework\MockObject\MockObject $container */
    $container = $this->createMock(ContainerInterface::class);
    $container
      ->expects($this->once())
      ->method('get')
      ->with('password')
      ->willReturn($passwordChecker);

    $processor = MigrateDcPlainPassword::create(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition
    );

    /** @var \Drupal\migrate\MigrateExecutable $migrateExecutable */
    $migrateExecutable = $this->createMock(MigrateExecutable::class);
    $row = new Row();
    $destinationProperty = NULL;

    static::assertSame(
      $expected,
      $processor->transform(
        $value,
        $migrateExecutable,
        $row,
        $destinationProperty
      )
    );
  }

}
