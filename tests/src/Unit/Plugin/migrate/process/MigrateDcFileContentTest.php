<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\migrate_dc\Plugin\migrate\process\MigrateDcFileContent;
use Drupal\Tests\UnitTestCase;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\process\MigrateDcFileContent<extended>
 */
class MigrateDcFileContentTest extends UnitTestCase {

  public function casesTransformSuccess(): array {
    $fileContent = 'my file content';
    $source = base64_encode($fileContent);

    return [
      'file exists' => [
        $fileContent,
        [
          'path' => 'data:/',
          'file_name' => "text/plain;base64,$source",
        ],
        '',
      ],
      'file exists with @ in path' => [
        $fileContent,
        [
          'path' => '@data:/',
          'file_name' => "text/plain;base64,$source",
        ],
        'data:/',
      ],
      'file does not exist' => [
        '',
        [
          'path' => 'foo',
          'file_name' => 'bar',
        ],
        'data:/',
      ],
    ];
  }

  /**
   * @dataProvider casesTransformSuccess
   */
  public function testTransformSuccess($expected, $configuration, $destination) {
    $pluginId = '';
    $pluginDefinition = [];
    $value = '';

    /** @var \Drupal\migrate\MigrateExecutable $migrateExecutable */
    $migrateExecutable = $this->createMock(MigrateExecutable::class);
    $row = new Row();
    $row->setDestinationProperty($destination, $destination);
    $destinationProperty = NULL;

    $processor = new MigrateDcFileContent(
      $configuration,
      $pluginId,
      $pluginDefinition
    );

    static::assertSame(
      $expected,
      $processor->transform(
        $value,
        $migrateExecutable,
        $row,
        $destinationProperty
      )
    );
  }

  public function casesTransformFail(): array {
    return [
      'no file in config' => [
        MigrateException::class,
        [],
        '',
      ],
    ];
  }

  /**
   * @dataProvider casesTransformFail
   */
  public function testTransformFail($expected, $configuration, $destination) {
    $pluginId = '';
    $pluginDefinition = [];
    $value = '';

    /** @var \Drupal\migrate\MigrateExecutable $migrateExecutable */
    $migrateExecutable = $this->createMock(MigrateExecutable::class);
    $row = new Row();
    $row->setDestinationProperty($destination, $destination);
    $destinationProperty = NULL;

    $processor = new MigrateDcFileContent(
      $configuration,
      $pluginId,
      $pluginDefinition
    );

    $this->expectException($expected);
    $this->expectExceptionMessage('Config path value does not exist');

    $processor->transform(
      $value,
      $migrateExecutable,
      $row,
      $destinationProperty
    );
  }

}
