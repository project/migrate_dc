<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\source;

use Drupal\Component\Serialization\YamlSymfony as Yaml;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_dc\Plugin\migrate\source\MigrateDcYamlSource;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\source\MigrateDcYamlSource<extended>
 */
class MigrateDcYamlSourceTest extends MigrateDcJsonSourceTest {

  /**
   * {@inheritDoc}
   */
  protected $serializerClass = Yaml::class;

  /**
   * {@inheritDoc}
   */
  protected function createInstance(array $configuration): MigrateSourceInterface {
    $pluginId = 'migrate_dc_yaml';
    $pluginDefinition = [];

    /** @var \Drupal\migrate\Plugin\MigrationInterface|\PHPUnit\Framework\MockObject\MockObject $migration */
    $migration = $this->createMock(MigrationInterface::class);

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface|\PHPUnit\Framework\MockObject\MockObject $container */
    $container = $this->createMock(ContainerInterface::class);
    $container
      ->method('get')
      ->with('serialization.yaml')
      ->willReturn($this->getSerializer());

    return MigrateDcYamlSource::create(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition,
      $migration
    );
  }

}
