<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_dc\Unit\Plugin\migrate\source;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_dc\Plugin\migrate\source\MigrateDcJsonSource;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group migrate_dc
 *
 * @covers \Drupal\migrate_dc\Plugin\migrate\source\MigrateDcJsonSource<extended>
 */
class MigrateDcJsonSourceTest extends UnitTestCase {

  /**
   * @var string
   */
  protected $serializerClass = Json::class;

  /**
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  protected function getSerializer(): SerializationInterface {
    if ($this->serializer === NULL) {
      $this->serializer = new $this->serializerClass();
    }

    return $this->serializer;
  }

  public function casesConstructorSuccess(): array {
    $serializer = $this->getSerializer();

    $source = base64_encode($serializer->encode([
      'id-01' => [
        'a' => 'A',
        'b' => 'B',
      ],
    ]));

    return [
      'basic' => [
        [
          'items' => [
            'id-01' => [
              'a' => 'A',
              'b' => 'B',
              'id' => 'id-01',
            ],
          ],
        ],
        [
          'fileDir' => 'data:/',
          'fileName' => "text/plain;base64,$source",
          'fields' => [
            'a' => 'Field A',
            'b' => 'Field B',
            'id' => 'Field ID',
          ],
          'identifier' => 'id',
        ],
      ],
      'identifier array' => [
        [
          'items' => [
            'id-01' => [
              'a' => 'A',
              'b' => 'B',
              'id' => 'id-01',
            ],
          ],
        ],
        [
          'fileDir' => 'data:/',
          'fileName' => "text/plain;base64,$source",
          'fields' => [
            'a' => 'Field A',
            'b' => 'Field B',
            'id' => 'Field ID',
          ],
          'identifier' => [
            'id' => [
              'type' => 'string',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider casesConstructorSuccess
   */
  public function testConstructorSuccess(array $expected, array $configuration) {
    $source = $this->createInstance($configuration);

    static::assertSameSize($expected['items'], $source);

    while ($source->valid()) {
      $id = $source->key();
      $item = $source->current();

      static::assertSame($expected['items'][$id], $item);
      $source->next();
    }

    static::assertSame($configuration['fields'], $source->fields());
  }

  protected function createInstance(array $configuration): MigrateSourceInterface {
    $pluginId = 'migrate_dc_json';
    $pluginDefinition = [];

    /** @var \Drupal\migrate\Plugin\MigrationInterface|\PHPUnit\Framework\MockObject\MockObject $migration */
    $migration = $this->createMock(MigrationInterface::class);

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface|\PHPUnit\Framework\MockObject\MockObject $container */
    $container = $this->createMock(ContainerInterface::class);
    $container
      ->method('get')
      ->with('serialization.json')
      ->willReturn($this->getSerializer());

    return MigrateDcJsonSource::create(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition,
      $migration
    );
  }

}
