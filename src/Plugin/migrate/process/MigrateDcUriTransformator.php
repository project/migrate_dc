<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\process;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @\Drupal\migrate\Annotation\MigrateProcessPlugin(
 *   id = "migrate_dc_uri_transformator"
 * )
 */
class MigrateDcUriTransformator extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function transform(
    $value,
    MigrateExecutableInterface $migrate_executable,
    Row $row,
    $destination_property
  ) {
    $uriParts = parse_url($value);
    if (isset($uriParts['scheme'])
      && $uriParts['scheme'] === 'entity.uuid'
      && !empty($uriParts['path'])
    ) {
      list($entityTypeId, $uuid) = explode('/', $uriParts['path']);
      $entityId = $this->getEntityIdFromUuid($entityTypeId, $uuid);
      if ($entityId) {
        $value = "entity:$entityTypeId/$entityId";
      }
    }

    return $value;
  }

  protected function getEntityIdFromUuid(string $entityTypeId, string $uuid): ?string {
    $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
    if (!$entityType->hasKey('uuid')) {
      return NULL;
    }

    $result = $this
      ->entityTypeManager
      ->getStorage($entityTypeId)
      ->getQuery()
      ->condition($entityType->getKey('uuid'), $uuid)
      ->execute();

    return $result ? reset($result) : NULL;
  }

}
