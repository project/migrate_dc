<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\process;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;

/**
 * Creates Shipment item.
 *
 * @\Drupal\migrate\Annotation\MigrateProcessPlugin(
 *   id = "migrate_dc_shipment_item",
 *   handle_multiples = TRUE
 * )
 */
class MigrateDcShipmentItem extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   *
   * Source data:
   * @code
   * shipment-01:
   *   uuid: 12345678-1234-1234-1234-123456789012
   *   order_id: order-01
   *   title: 'My title 01'
   *   shipping_method: delivery
   *   shipping_profile:
   *     -
   *       target_id: 'shipping_address-profile-01'
   *   items:
   *     -
   *       title: 'my title 01'
   *       order_item_id: 'order-09-01'
   *       quantity: '1'
   *       weight:
   *         number: 0
   *         unit: 'kg'
   *       declared_value:
   *         number: '120'
   *         currency_code: 'HUF'
   * @endcode
   *
   * Migration process definition:
   * @code
   * process:
   *   items:
   *     -
   *       source: items
   *       plugin: sub_process
   *       process:
   *         order_item_id:
   *           source: order_item_id
   *           plugin: migration_lookup
   *           migration:
   *             - my_commerce_order_item_migration_id
   *         title: title
   *         quantity: quantity
   *         weight:
   *           source: weight
   *           plugin: default_value
   *           default_value:
   *             number: 0
   *             unit: 'g'
   *         declared_value:
   *           source: declared_value
   *           plugin: default_value
   *           default_value:
   *             number: '0'
   *             currency_code: 'USD'
   *     -
   *       plugin: migrate_dc_shipment_item
   * @endcode
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $shipmentItems = [];
    foreach ((array) $value as $key => $input) {
      $orderItemId = $input['order_item_id'] ?? NULL;
      $input = array_replace_recursive(
        $this->getDefaultInput($orderItemId),
        $input,
      );

      $shipmentItems[$key] = new ShipmentItem([
        'order_item_id' => $orderItemId,
        'title' => $input['title'],
        'quantity' => $input['quantity'],
        'weight' => new Weight(
          (string) $input['weight']['number'],
          (string) $input['weight']['unit'],
        ),
        'declared_value' => new Price(
          (string) $input['declared_value']['number'],
          (string) $input['declared_value']['currency_code'],
        ),
      ]);
    }

    return $shipmentItems;
  }

  protected function getDefaultInput($orderItemId): array {
    return [
      'title' => '',
      'quantity' => 1,
      'weight' => [
        'number' => '0',
        'unit' => WeightUnit::KILOGRAM,
      ],
      'declared_value' => [
        'number' => '0',
        'currency_code' => $this->getCurrencyCodeFromOrderItem($orderItemId),
      ],
    ];
  }

  /**
   * @param null|int|string $orderItemId
   */
  protected function getCurrencyCodeFromOrderItem($orderItemId): string {
    // @todo
    return 'USD';
  }

}
