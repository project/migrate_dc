<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\process;

use Drupal\Component\Utility\NestedArray;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use RuntimeException;

/**
 * This plugin creates a unix-timestamps from date strings.
 *
 * @\Drupal\migrate\Annotation\MigrateProcessPlugin(
 *   id = "migrate_dc_file_content"
 * )
 */
class MigrateDcFileContent extends ProcessPluginBase {

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $configuration += [
      'path' => '',
      'file_name' => '',
    ];

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $destination = $row->getDestination();

    $path = $this->getConfigValue('path', $destination);
    $fileName = $this->getConfigValue('file_name', $destination);
    $filePath = "$path/$fileName";

    try {
      $content = $this->fileGetContents($filePath);
    }
    catch (RuntimeException $e) {
      $content = '';
    }

    return $content;
  }

  protected function getConfigValue(string $key, array $destination) {
    $keyExists = FALSE;
    $value = $this->configuration[$key];
    if (mb_strpos($value, '@') === 0) {
      $value = NestedArray::getValue(
        $destination,
        explode(Row::PROPERTY_SEPARATOR, mb_substr($value, 1)),
        $keyExists
      );
    }

    if (!$value) {
      throw new MigrateException("Config $key value does not exist", 1);
    }
    return $value;
  }

  protected function fileGetContents(string $fileName): string {
    $content = @file_get_contents($fileName);
    if ($content === FALSE) {
      throw new RuntimeException("file '$fileName' count not be read", 1);
    }

    return $content;
  }

}
