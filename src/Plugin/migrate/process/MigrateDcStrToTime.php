<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\process;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This plugin creates a unix-timestamps from date strings.
 *
 * @\Drupal\migrate\Annotation\MigrateProcessPlugin(
 *   id = "migrate_dc_str_to_time"
 * )
 */
class MigrateDcStrToTime extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('datetime.time'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TimeInterface $time,
    DateFormatterInterface $dateFormatter
  ) {
    $this->time = $time;
    $this->dateFormatter = $dateFormatter;

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $config = array_replace_recursive($this->getDefaultConfiguration(), $this->configuration);

    $unixTimestamp = $value;
    if (!preg_match('/^\d+$/', (string) $unixTimestamp)) {
      $requestTime = $this->time->getRequestTime();

      $baseTime = $config['base_time'] === 'now' ? $requestTime : strtotime($config['base_time'], $requestTime);
      $unixTimestamp = strtotime($unixTimestamp, $baseTime);
    }

    if ($config['output_format'] === 'U') {
      return $unixTimestamp;
    }

    return $this->dateFormatter->format(
      $unixTimestamp,
      $config['output_type'],
      $config['output_format'],
      $config['output_timezone'],
      $config['output_langcode']
    );
  }

  protected function getDefaultConfiguration(): array {
    return [
      'base_time' => 'now',
      'output_type' => 'custom',
      'output_format' => 'U',
      'output_timezone' => NULL,
      'output_langcode' => NULL,
    ];
  }

}
