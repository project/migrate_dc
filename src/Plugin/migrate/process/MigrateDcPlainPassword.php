<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\process;

use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Converts plain password to hashes.
 *
 * @\Drupal\migrate\Annotation\MigrateProcessPlugin(
 *   id = "migrate_dc_plain_password"
 * )
 */
class MigrateDcPlainPassword extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('password')
    );
  }

  /**
   * Password manager.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected $passwordChecker = NULL;

  /**
   * Default password.
   *
   * @var string
   */
  protected $defaultValue = 'a';

  /**
   * Constructs a MachineName plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Password\PasswordInterface $password_checker
   *   The password manager instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PasswordInterface $password_checker) {
    $this->passwordChecker = $password_checker;
    $configuration += ['default_value' => $this->defaultValue];

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!$value) {
      $value = $this->configuration['default_value'];
    }

    return $this->passwordChecker->hash($value);
  }

}
