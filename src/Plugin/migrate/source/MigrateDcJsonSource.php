<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\source;

use ArrayIterator;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Reads source from a JSON file.
 *
 * @\Drupal\migrate\Annotation\MigrateSource(
 *   id = "migrate_dc_json"
 * )
 */
class MigrateDcJsonSource extends SourcePluginBase implements ContainerFactoryPluginInterface {

  /**
   * Path to the directory which contains the source file.
   *
   * @var string
   */
  protected $fileDir = '';

  /**
   * File name relative from $this->fileDir.
   *
   * @var string
   */
  protected $fileName = '';

  /**
   * An array of source fields.
   *
   * @var array
   */
  protected $fields = [];

  /**
   * The field name that is a unique identifier.
   *
   * @var string|array
   */
  protected $identifier = '';

  /**
   * Items to import.
   *
   * @var array
   */
  protected $items = NULL;

  /**
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * @var string[]
   */
  protected $requiredConfigKeys = [
    'fileDir',
    'fileName',
    'fields',
    'identifier',
  ];

  /**
   * @var array
   */
  protected $configKeyMapping = [
    'fileDir' => 'fileDir',
    'fileName' => 'fileName',
    'fields' => 'fields',
    'identifier' => 'identifier',
  ];

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration = NULL
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('serialization.json')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    SerializationInterface $serializer
  ) {
    $this->serializer = $serializer;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this
      ->assertConfiguration()
      ->setPropertiesFromConfiguration();
  }

  /**
   * @return $this
   */
  protected function assertConfiguration() {
    $missingKeys = array_diff(
      $this->requiredConfigKeys,
      array_keys($this->configuration)
    );

    assert(
      count($missingKeys) === 0,
      'Source configuration must include the following keys: ' . implode(', ', $missingKeys)
    );

    return $this;
  }

  protected function setPropertiesFromConfiguration() {
    foreach ($this->configKeyMapping as $property => $config) {
      if (array_key_exists($config, $this->configuration)) {
        $this->$property = $this->configuration[$config];
      }
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function count($refresh = FALSE) {
    return count($this->getItems());
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    if (is_array($this->identifier)) {
      return $this->identifier;
    }

    return [
      $this->identifier => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function __toString() {
    return $this->getFilePath();
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    return $this->fields;
  }

  /**
   * Get protected values.
   *
   * @todo Do we need this?
   *
   * @param string $property
   *   Property name.
   *
   * @return mixed
   *   Value of the property.
   */
  public function get(string $property) {
    return $this->{$property};
  }

  /**
   * Get all items.
   *
   * @return array
   *   All items.
   */
  protected function getItems(): array {
    if ($this->items === NULL) {
      $this->initItems();
    }

    return $this->items;
  }

  protected function initItems() {
    $this->items = $this->serializer->decode($this->fileGetContents($this->getFilePath()));
    $ids = $this->getIds();
    if (count($ids) === 1 && key($this->items) !== 0) {
      $idField = key($ids);
      foreach (array_keys($this->items) as $id) {
        $this->items[$id][$idField] = $id;
      }
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function initializeIterator() {
    return new ArrayIterator($this->getItems());
  }

  protected function getFilePath(): string {
    return "{$this->fileDir}/{$this->fileName}";
  }

  protected function fileGetContents(string $fileName): string {
    $content = file_get_contents($fileName);
    if ($content === FALSE) {
      throw new RuntimeException("file '$fileName' count not be read", 1);
    }

    return $content;
  }

}
