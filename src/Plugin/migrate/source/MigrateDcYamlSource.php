<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Reads source from a Yaml file.
 *
 * @\Drupal\migrate\Annotation\MigrateSource(
 *   id = "migrate_dc_yaml"
 * )
 */
class MigrateDcYamlSource extends MigrateDcJsonSource {

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration = NULL
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('serialization.yaml')
    );
  }

}
