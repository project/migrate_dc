<?php

declare(strict_types = 1);

namespace Drupal\migrate_dc\Plugin\migrate_plus\data_parser;

use ArrayIterator;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\DataParserPluginBase;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Obtain Yaml data for migration.
 *
 * @\Drupal\migrate_plus\Annotation\DataParser(
 *   id = "migrate_dc_yaml",
 *   title = @\Drupal\Core\Annotation\Translation("Yaml")
 * )
 */
class MigrateDcYamlParser extends DataParserPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Iterator over the Yaml data.
   *
   * @var \Iterator
   */
  protected $iterator;

  /**
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('serialization.yaml')
    );
  }

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SerializationInterface $serializer
  ) {
    $this->serializer = $serializer;

    if (empty($configuration['ids'])) {
      throw new MigrateException('You must declare "ids" as a unique array of fields in your source settings.');
    }

    $configuration += [
      'item_selector' => '',
    ];

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Retrieves the Yaml data and returns it as an array.
   *
   * @param string $url
   *   URL of a Yaml feed.
   *
   * @return array
   *   The selected data to be iterated.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   */
  protected function getSourceData($url) {
    $response = $this->getDataFetcherPlugin()->getResponseContent($url);
    $sourceData = $this->serializer->decode($response);

    if (is_int($this->itemSelector)) {
      return $this->selectByDepth($sourceData);
    }

    // Otherwise, we're using xpath-like selectors.
    $selectors = array_filter(explode('/', trim($this->itemSelector, '/')));
    foreach ($selectors as $selector) {
      $sourceData = $sourceData[$selector];
    }

    return $sourceData;
  }

  /**
   * Get the source data for reading.
   *
   * @param array $rawData
   *   Raw data from the Yaml feed.
   *
   * @return array
   *   Selected items at the requested depth of the Yaml feed.
   */
  protected function selectByDepth(array $rawData) {
    // Return the results in a recursive iterator that can traverse
    // multidimensional arrays.
    $iterator = new RecursiveIteratorIterator(
      new RecursiveArrayIterator($rawData),
      RecursiveIteratorIterator::SELF_FIRST
    );

    $items = [];
    // Backwards-compatibility - an integer item_selector is interpreted as a
    // depth.
    // When there is an array of items at the expected depth, pull that array
    // out as a distinct item.
    $identifierDepth = $this->itemSelector;
    $iterator->rewind();
    while ($iterator->valid()) {
      $item = $iterator->current();
      if (is_array($item) && $iterator->getDepth() == $identifierDepth) {
        $items[] = $item;
      }

      $iterator->next();
    }

    return $items;
  }

  /**
   * {@inheritDoc}
   */
  protected function openSourceUrl($url) {
    // (Re)open the provided URL.
    $source_data = $this->getSourceData($url);
    $this->iterator = new ArrayIterator($source_data);

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  protected function fetchNextRow() {
    $current = $this->iterator->current();
    if ($current) {
      foreach ($this->fieldSelectors() as $fieldName => $selector) {
        $field_data = $current;
        $fieldSelectors = explode('/', trim($selector, '/'));
        foreach ($fieldSelectors as $fieldSelector) {
          $field_data = $field_data[$fieldSelector];
        }
        $this->currentItem[$fieldName] = $field_data;
      }

      if (!empty($this->configuration['include_raw_data'])) {
        $this->currentItem['raw'] = $current;
      }

      $this->iterator->next();
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function fieldSelectors() {
    $fields = [];
    foreach ($this->configuration['fields'] as $key => $fieldInfo) {
      if (!is_array($fieldInfo)) {
        $fields[$key] = $key;
      }
      elseif (isset($fieldInfo['selector'])) {
        $fields[$fieldInfo['name']] = $fieldInfo['selector'];
      }
    }

    return $fields;
  }

}
